package net.pl3x.bukkit.pistonblocker.listener;

import net.pl3x.bukkit.pistonblocker.Logger;
import net.pl3x.bukkit.pistonblocker.configuration.Config;
import net.pl3x.bukkit.pistonblocker.configuration.Log;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockPistonExtendEvent;
import org.bukkit.event.block.BlockPistonRetractEvent;
import org.bukkit.inventory.ItemStack;

import java.sql.Timestamp;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

public class PistonListener implements Listener {
    private final Set<Material> duplicator = new HashSet<>();
    private final Set<Material> duplicatable = new HashSet<>();

    public PistonListener() {
        duplicator.add(Material.SLIME_BLOCK);
        duplicator.add(Material.MELON_BLOCK);
        duplicator.add(Material.PUMPKIN);
        duplicator.add(Material.JACK_O_LANTERN);
        duplicator.add(Material.LEAVES);
        duplicator.add(Material.LEAVES_2);

        duplicatable.add(Material.ACTIVATOR_RAIL);
        duplicatable.add(Material.DETECTOR_RAIL);
        duplicatable.add(Material.POWERED_RAIL);
        duplicatable.add(Material.RAILS);
        duplicatable.add(Material.CARPET);
    }


    @EventHandler(priority = EventPriority.HIGHEST, ignoreCancelled = true)
    public void onPistonRetract(BlockPistonRetractEvent event) {
        if (detectedRailDupe(event.getBlock(), event.getDirection(), event.getBlocks(), event.isSticky())) {
            event.setCancelled(true);
        }
    }

    @EventHandler(priority = EventPriority.HIGHEST, ignoreCancelled = true)
    public void onPistonExpand(BlockPistonExtendEvent event) {
        if (detectedRailDupe(event.getBlock(), event.getDirection(), event.getBlocks(), event.isSticky())) {
            event.setCancelled(true);
        }
    }

    private boolean detectedRailDupe(Block piston, BlockFace direction, List<Block> blocks, boolean sticky) {
        for (Block block : blocks) {
            if (!duplicatable.contains(block.getType())) {
                continue; // cant duplicate this
            }

            if (!duplicator.contains(block.getRelative(direction).getType()) &&
                    !duplicator.contains(block.getRelative(BlockFace.DOWN).getType())) {
                continue; // not pushing against or sitting on duplicator
            }

            if (Config.BREAK_PISTON) {
                piston.setType(Material.AIR);
                if (Config.DROP_PISTON) {
                    // drop piston to ground
                    piston.getWorld().dropItemNaturally(piston.getLocation(),
                            new ItemStack(sticky ? Material.PISTON_STICKY_BASE : Material.PISTON_BASE));
                }
                if (Config.EXPLOSION_EFFECT) {
                    // create explosion effect
                    piston.getWorld().createExplosion(piston.getLocation(), (float) Config.EXPLOSION_POWER);
                }
            }

            Location loc = piston.getLocation();
            Set<String> nearbyPlayers = loc.getWorld().getNearbyEntities(loc, 25, 25, 25).stream()
                    .filter(entity -> entity instanceof Player).map(CommandSender::getName)
                    .collect(Collectors.toCollection(HashSet::new));
            String log = "Duplication attempt detected at " + loc.getWorld().getName() + " " +
                    loc.getBlockX() + "," + loc.getBlockY() + "," + loc.getBlockZ() + " | Nearby players: " + nearbyPlayers;
            Log.write("[" + new Timestamp(new Date().getTime()) + "] " + log);
            Logger.warn(log);
            if (Config.RUN_COMMAND != null && !Config.RUN_COMMAND.isEmpty()) {
                Bukkit.dispatchCommand(Bukkit.getConsoleSender(), Config.RUN_COMMAND
                        .replace("{world}", loc.getWorld().getName())
                        .replace("{x}", Integer.toString(loc.getBlockX()))
                        .replace("{y}", Integer.toString(loc.getBlockY()))
                        .replace("{z}", Integer.toString(loc.getBlockZ()))
                        .replace("{nearby}", nearbyPlayers.toString()));
            }

            return true;
        }
        return false;
    }
}
