package net.pl3x.bukkit.pistonblocker;

import net.pl3x.bukkit.pistonblocker.command.CmdPistonBlocker;
import net.pl3x.bukkit.pistonblocker.configuration.Config;
import net.pl3x.bukkit.pistonblocker.configuration.Lang;
import net.pl3x.bukkit.pistonblocker.configuration.Log;
import net.pl3x.bukkit.pistonblocker.listener.PistonListener;
import org.bukkit.event.Listener;
import org.bukkit.plugin.java.JavaPlugin;

public class PistonBlocker extends JavaPlugin implements Listener {
    @Override
    public void onEnable() {
        Config.reload();
        Lang.reload();
        Log.init();

        getServer().getPluginManager().registerEvents(new PistonListener(), this);

        getCommand("pistonblocker").setExecutor(new CmdPistonBlocker(this));

        Logger.info(getName() + " v" + getDescription().getVersion() + " enabled!");
    }

    @Override
    public void onDisable() {
        Logger.info(getName() + " disabled.");
    }

    public static PistonBlocker getPlugin() {
        return PistonBlocker.getPlugin(PistonBlocker.class);
    }
}
