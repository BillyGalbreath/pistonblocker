package net.pl3x.bukkit.pistonblocker.configuration;

import net.pl3x.bukkit.pistonblocker.PistonBlocker;
import org.bukkit.configuration.file.FileConfiguration;

public class Config {
    public static boolean COLOR_LOGS = true;
    public static boolean DEBUG_MODE = false;
    public static String LANGUAGE_FILE = "lang-en.yml";
    public static String RUN_COMMAND = "";
    public static boolean BREAK_PISTON = true;
    public static boolean DROP_PISTON = false;
    public static boolean EXPLOSION_EFFECT = true;
    public static double EXPLOSION_POWER = 4.0;

    public static void reload() {
        PistonBlocker plugin = PistonBlocker.getPlugin();
        plugin.saveDefaultConfig();
        plugin.reloadConfig();
        FileConfiguration config = plugin.getConfig();

        COLOR_LOGS = config.getBoolean("color-logs", true);
        DEBUG_MODE = config.getBoolean("debug-mode", false);
        LANGUAGE_FILE = config.getString("language-file", "lang-en.yml");
        RUN_COMMAND = config.getString("run-command", "");
        BREAK_PISTON = config.getBoolean("break-piston", true);
        DROP_PISTON = config.getBoolean("drop-piston", false);
        EXPLOSION_EFFECT = config.getBoolean("explosion-effect", true);
        EXPLOSION_POWER = config.getDouble("explosion-power", 4.0);
    }
}
