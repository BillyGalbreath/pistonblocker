package net.pl3x.bukkit.pistonblocker.configuration;

import net.pl3x.bukkit.pistonblocker.Logger;
import net.pl3x.bukkit.pistonblocker.PistonBlocker;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

public class Log {
    private static File logFile;

    public static void init() {
        logFile = new File(PistonBlocker.getPlugin().getDataFolder(), "log.txt");

        if (!logFile.exists()) {
            try {
                if (!logFile.createNewFile()) {
                    logFile = null;
                }
            } catch (IOException e) {
                Logger.error("Could not create new file log.txt! See stacktrace for details:");
                e.printStackTrace();
                logFile = null;
            }
        }
    }

    public static void write(String message) {
        if (logFile == null) {
            Logger.warn("Log file not initialized!");
            return;
        }

        try {
            BufferedWriter output = new BufferedWriter(new FileWriter(logFile, true));
            output.append(message);
            output.newLine();
            output.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
